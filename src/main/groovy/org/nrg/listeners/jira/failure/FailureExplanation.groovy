package org.nrg.listeners.jira.failure

class FailureExplanation implements FailureCause {

    private String reason

    FailureExplanation(String reason) {
        this.reason = reason
    }

    @Override
    String getHTMLReason() {
        reason
    }

}
