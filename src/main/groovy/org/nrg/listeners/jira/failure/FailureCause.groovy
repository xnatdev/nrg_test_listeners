package org.nrg.listeners.jira.failure

interface FailureCause {

    String getHTMLReason()

}
